module "eks" {
  source            = "./modules/eks"
  vpc_id            = 	"vpc-0e10048141d6c8289"
  cluster_name      = var.cluster_name
  eks_cluster       = module.eks.eks_cluster_name
  instance_types    = var.instance_types
  node_group_name   = var.node_group_name
  eks_version       = var.eks_version
  security_group_id = module.sg.eks-cluster-sg
  subnet_ids = ["subnet-0ad19375727e58376", "subnet-0e9b46ce8896e3e9c", "subnet-085db5f15939c2365"]

}

